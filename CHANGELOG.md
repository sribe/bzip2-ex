# Change log

## 1.0.0-TODO
Major release milestones:
* Parallel decompression.

## 0.2.0 (Sep 2022)
* First working release.
* Passing CI for elixir 1.11 and 1.14

## 0.1.0 (May 2022)
* Broken release, missing the Bzip2.Driver module.
