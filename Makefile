ifeq ($(MIX_TARGET),)
  $(error This Makefile can only be run during `mix compile`.)
endif

CFLAGS += -g -O3
CFLAGS += -ansi -std=c99 -pedantic -Wall -Wextra -Wno-unused-parameter
CFLAGS += -I$(ERL_EI_INCLUDE_DIR)

LDFLAGS += -lbz2

LDFLAGS += -fPIC -shared
ifeq ($(CROSSCOMPILE),)
  ifeq ($(shell uname),Darwin)
    LDFLAGS += -undefined dynamic_lookup
  endif
endif

BUILD_DIR = $(MIX_APP_PATH)/priv
OBJECT = $(BUILD_DIR)/bzip2.so

.PHONY: all

# Magic "@" will prevent printing unnecessary lines when nothing needs to be
# recompiled.
all: $(OBJECT)
	@:

$(OBJECT): $(BUILD_DIR) src/bzip2.c
	$(CC) $(CFLAGS) -o $@ src/bzip2.c $(LDFLAGS)

$(BUILD_DIR):
	mkdir -p $@
