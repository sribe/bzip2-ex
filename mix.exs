defmodule Bzip2.MixProject do
  use Mix.Project

  def project do
    [
      app: :bzip2,
      version: "0.2.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: "Streaming bzip2 codec",
      compilers: [:elixir_make] ++ Mix.compilers(),
      source_url: "https://gitlab.com/adamwight/bzip2-ex",
      docs: [
        extras: ~w(CHANGELOG.md README.md),
        main: "readme"
      ],
      package: package()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:elixir_make, "~> 0.6", runtime: false},
      {:ex_doc, "~> 0.0", only: :dev, runtime: false}
    ]
  end

  defp package do
    [
      files: ~w(lib mix.exs *.md src Makefile),
      name: :bzip2,
      maintainers: ["adamwight"],
      licenses: ["GPLv3"],
      links: %{"GitLab" => "https://gitlab.com/adamwight/bzip2-ex"}
    ]
  end
end
