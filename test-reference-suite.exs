defmodule Bz2RefTest do
  defp find_suffix(dir_path, suffix) do
    File.ls!(dir_path)
    |> Enum.flat_map(fn path ->
      full_path = Path.join([dir_path, path])

      File.stat!(full_path)
      |> case do
        %{type: :directory} -> find_suffix(full_path, suffix)
        _ -> [full_path]
      end
    end)
    |> Enum.filter(fn path -> String.ends_with?(path, suffix) end)
  end

  defp decompress(path) do
    File.read!(path)
    |> Bzip2.decompress!()
  end

  defp decompress_to_hash(path) do
    decompress(path)
    |> then(&:crypto.hash(:md5, &1))
    |> Base.encode16()
    |> String.downcase()
  end

  defp expected_hash_for(path) do
    path
    |> String.replace_suffix(".bz2", ".md5")
    |> File.read!()
    |> String.slice(0..31)
  end

  defp verify_all_good(tests_path) do
    files = find_suffix(tests_path, ".bz2")

    Enum.each(files, fn path ->
      try do
        ExUnit.Assertions.assert decompress_to_hash(path) == expected_hash_for(path),
          "Decompressed #{path} doesn't match expected md5."
      rescue
        e ->
          IO.puts("BAD - failure decompressing #{path}")
          IO.puts(Exception.format(:error, e, __STACKTRACE__))
      end
    end)

    IO.puts("Tested against #{inspect(Enum.count(files))} good files")
  end

  defp verify_all_bad(tests_path) do
    files = find_suffix(tests_path, ".bz2.bad")

    Enum.each(files, fn path ->
      try do
        decompress(path)
        IO.puts("BAD - Should not have decompressed #{path} successfully.")
      rescue
        ErlangError -> nil
      end
    end)

    IO.puts("Tested against #{inspect(Enum.count(files))} known bad files")
  end

  def verify_all(tests_path) do
    if not File.exists?(Path.join(tests_path, "run-tests.sh")) do
      raise "Not a valid bzip2-tests directory: #{tests_path}"
    end

    verify_all_good(tests_path)
    verify_all_bad(tests_path)
  end
end

OptionParser.parse!(System.argv(), strict: [])
|> case do
  {_, [tests_path]} -> Bz2RefTest.verify_all(tests_path)
  _ -> IO.puts("Usage: mix run test-reference-suite.exs ../bzip-tests/")
end
