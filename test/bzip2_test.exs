defmodule Bzip2Test do
  use ExUnit.Case
  doctest Bzip2

  test "decompress" do
    data = File.read!("test/data/text1.bz2")
    expected = File.read!("test/data/text1")
    assert Bzip2.decompress!(data) === expected
  end

  test "decompress with EOFs and newlines" do
    data = File.read!("test/data/text-eof.bz2")
    expected = File.read!("test/data/text-eof")
    assert Bzip2.decompress!(data) === expected
  end

  test "reject non-bz2 data" do
    assert_raise ErlangError, ~r/:bz2_data_error_magic/, fn ->
      Bzip2.decompress!("foo")
    end
  end

  test "reject corrupt bz2 data" do
    data = File.read!("test/data/corrupt.bz2")

    assert_raise ErlangError, ~r/:bz2_data_error/, fn ->
      Bzip2.decompress!(data)
    end
  end

  test "decompress multistream data" do
    data = File.read!("test/data/multistream.bz2")
    expected = File.read!("test/data/multistream")
    assert Bzip2.decompress!(data) === expected
  end

  test "compress" do
    data = File.read!("test/data/text1")
    expected = File.read!("test/data/text1.bz2")
    assert Bzip2.compress!(data) === expected
  end

  test "compress with EOFs and newlines" do
    data = File.read!("test/data/text-eof")
    expected = File.read!("test/data/text-eof.bz2")
    assert Bzip2.compress!(data) === expected
  end

  test "compress multistream data" do
    data =
      Stream.chunk_every(["abc", "defg"], 1)
      |> Bzip2.compress!()
      |> Enum.to_list()
      |> Enum.join("")
      |> Bzip2.decompress!()

    assert data === "abcdefg"
  end
end
