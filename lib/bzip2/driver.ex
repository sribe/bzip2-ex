defmodule Bzip2.Driver do
  # Only load the NIF library on the first usage.
  @compile {:autoload, false}
  @on_load :init

  @spec init :: :ok | (error :: any())
  def init() do
    Application.app_dir(:bzip2, ["priv", "bzip2"])
    |> :erlang.load_nif(0)
  end

  # Fallbacks

  @spec compress_init!() :: state :: reference()
  def compress_init! do
    exit(:nif_library_not_loaded)
  end

  @type compression_status :: :cont | :next_input | :stream_end

  @spec compress_block!(state :: reference(), data :: binary()) ::
          {:ok, compression_status(), binary()}
  def compress_block!(_, _) do
    exit(:nif_library_not_loaded)
  end

  @spec compress_end!(state :: reference()) :: :ok
  def compress_end!(_) do
    exit(:nif_library_not_loaded)
  end

  @spec decompress_init!() :: state :: reference()
  def decompress_init! do
    exit(:nif_library_not_loaded)
  end

  @type decompression_status ::
          :cont
          | :next_input
          | {:stream_end, unused :: integer()}

  @spec decompress_block!(state :: reference(), data :: binary()) ::
          {:ok, decompression_status(), binary()}
  def decompress_block!(_, _) do
    exit(:nif_library_not_loaded)
  end

  @spec decompress_end!(state :: reference()) :: :ok
  def decompress_end!(_) do
    exit(:nif_library_not_loaded)
  end
end
