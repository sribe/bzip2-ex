# Elixir bindings for libbz2

Streaming bzip2 codec.

## Installation

Your system must have the development package for bzip2.  For example,
```
apt install libbz2-dev
```

This package can be installed by adding `bzip2` to your list of dependencies in
`mix.exs`:

```elixir
def deps do
  [
    {:bzip2, "~> 0.2"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm).

Published docs are online at <https://hexdocs.pm/bzip2>.

## Usage

```elixir
File.stream!("/dev/zero", [], 1024 * 1024)
|> Stream.take(10)
|> Bzip2.compress!()
|> Enum.into("")
|> byte_size()
# 450
```

## Debugging

This repo includs some basic tests:
```
mix test
```

You can also run against the reference tests,

```
git clone https://sourceware.org/git/bzip2-tests.git
cd bzip2-ex
mix run test-reference-suite.exs ../bzip2-tests
```

To get verbose output from the NIF, change this line in `bzip2.c`:
```c
#define BZIP2_EX_DEBUG 1
```

## Known bugs

* Only builds on linux-x86_64, please send patches to port to other platforms!
* Cannot handle gaps or trash between streams.  See the reference tests `lbzip2/gap.bz2` and `lbzip2/trash.bz2`.
* Should throw an error when decompressing an empty file.  See the reference test `libbz2-dev`.
